<h3>Template Unique Name: {{ mc.template.unique_name }}</h3>
<table class="form-table">
    <tbody>
    <tr class="form-field form-required">
        <th scope="row">
            <label for="from-name"><span class="description">*</span> Sender Name: </label>
            <input ng-model="mc.template.from_name" type="text" id="from-name">
        </th>
    </tr>
    <tr class="form-field form-required">
        <th scope="row">
            <label for="from-email"><span class="description">*</span> Sender Email: </label>
            <input ng-model="mc.template.from_email" type="text" id="from-email">
        </th>
    </tr>
    <tr class="form-field form-required">
        <th scope="row">
            <label for="subject"><span class="description">*</span> Subject: </label>
            <input ng-model="mc.template.subject" type="text" id="subject">
        </th>
    </tr>
    <tr class="form-field form-required">
        <th scope="row">
            <label for="mail-body"><span class="description">*</span> Mail Body: </label>
            <textarea ng-model="mc.template.mail_body" id="mail-body" rows="8"></textarea>
        </th>
    </tr>
    </tbody>
</table>

<hr/>
<button type="submit" class="button button-primary">Update</button>