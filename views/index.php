<div ng-app="mcApp">
    <div ng-controller="MessageCenterController as mc">

        <h1><?= esc_html( get_admin_page_title() ); ?></h1>

        <hr/>

        <table class="wp-list-table widefat fixed striped">
            <thead>
            <tr>
                <th>Unique Name</th>
                <th>Sender Name</th>
                <th>Sender Email</th>
                <th>Subject</th>
                <th>Mail Body</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="tpt in mc.templates">
                <td>{{ tpt.unique_name }}</td>
                <td>{{ tpt.from_name }}</td>
                <td>{{ tpt.from_email }}</td>
                <td>{{ tpt.subject }}</td>
                <td>{{ tpt.mail_body | limitTo: 200 }}</td>
                <td><a href="#TB_inline?width=1000&height=600&inlineId=mc-tickbox-content-edit" class="thickbox" ng-click="mc.getTemplate(tpt.id)">Edit</a></td>
            </tr>
            </tbody>
        </table>

        <?php add_thickbox(); ?>
        <!-- Edit mail template with dynamic content -->
        <div id="mc-tickbox-content-edit" style="display: none;">
            <form ng-submit="mc.updateTemplate(mc.template.id)">

                <?php
                include '_form.php';
                ?>

            </form>
        </div>
        
    </div>
</div>