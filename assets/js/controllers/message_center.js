(function () {

    'use strict';

    angular.module('mcApp')
        .controller('MessageCenterController', MessageCenterController);

    MessageCenterController.$inject = ['$rootScope', 'MessageCenterService', '$window'];

    function MessageCenterController($rootScope, MessageCenterService, $window)
    {
        var vm = this;

        $rootScope.site_url = 'http://lykkeboadmin.dk/wp-json';

        vm.templates = {};
        vm.template = {};
        vm.error_message = '';

        MessageCenterService.getAll()
            .then(function (response) {
                vm.templates = response.data;
            }, function (error) {
                console.log('Get message templates: ');
                console.log(error);
            });

        vm.getTemplate = function (id) {
            MessageCenterService.getTemplate(id)
                .then(function (response) {
                    vm.template = response.data;
                }, function (error) {
                    console.log('Get message template by id: ');
                    console.log(error);
                });
        };

        vm.updateTemplate = function (id) {
            var data = {
                id: id,
                from_name: vm.template.from_name,
                from_email: vm.template.from_email,
                subject: vm.template.subject,
                mail_body: vm.template.mail_body
            };

            MessageCenterService.updateTemplate(id, data)
                .then(function (response) {
                    $window.location.href = '/wp-admin/admin.php?page=message-center';
                }, function (error) {
                    console.log('Update message template: ');
                    console.log(error);
                });
        };
    }

})();