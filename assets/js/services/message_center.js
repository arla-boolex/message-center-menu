(function () {

    'use strict';

    angular.module('mcApp')
        .factory('MessageCenterService', MessageCenterService);

    MessageCenterService.$inject = ['$rootScope', '$http'];

    function MessageCenterService($rootScope, $http)
    {
        var service = {};

        service.getAll = function () {
            return $http.get($rootScope.site_url + '/logbook/v1/mail_templates');
        };

        service.getTemplate = function (id) {
            return $http.get($rootScope.site_url + '/logbook/v1/mail_templates/' + id);
        };

        service.updateTemplate = function (id, data) {
            return $http.post($rootScope.site_url + '/logbook/v1/mail_templates/' + id, data);
        };

        return service;
    }

})();